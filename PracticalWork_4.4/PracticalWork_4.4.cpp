﻿// PracticalWork_4.4.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//

#include <iostream>
#include <algorithm>
#include <map>
#include <unordered_map>
#include <set>

using namespace std;

void AlgFind()
{
    // поиск зеро в мапе
    map<string, int> find_map;
    find_map["first"] = 1;
    find_map["second"] = 2;
    find_map["third"] = 3;
    find_map["fourth"] = 4;

    cout << "Map find zero" << endl;
    auto result = find_map.find("zero");
   if (result == find_map.end())
    {
        cout << "Zero not found" << endl;
    }
    else
    {
        cout << "Zero found" << endl;
    }
    
}

void AlgForEach()
{
    set<int> s;
    for (int i = 1; i <= 5; i++) 
    {
        s.insert(i);
    }
    for_each(s.begin(), s.end(), [](int n) { cout << n << endl; });
}

void AlgCountIf()
{
    // подсчёт second в мапе
    unordered_map<string, int> CountIf_map;
    CountIf_map["first"] = 1;
    CountIf_map["second"] = 2;
    CountIf_map["third"] = 3;
    CountIf_map["fourth"] = 4;

    cout << "Unordered map count second: " << count_if(CountIf_map.begin(), CountIf_map.end(),
        [](pair<string, int> CountIf_map) { return CountIf_map.first == "second"; }) << endl;
}

int main()
{
    AlgFind();
    AlgCountIf();
    AlgForEach();
}